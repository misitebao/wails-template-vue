# Changelog

All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v2.0.0-beta.5] - 2022-03-06

### Added

- Add `frontend:dev:watcher` by [@misitebao](https://github.com/misitebao), corresponding to wails cli version is [v2.0.0-beta.33](https://github.com/wailsapp/wails/releases/tag/v2.0.0-beta.33)

## [v2.0.0-beta.4] - 2022-02-23

### Added

- Add typescript template option by [@misitebao](https://github.com/misitebao)

### Changed

- Modify window size by [@crushonyou18](https://github.com/crushonyou18)

[unreleased]: https://github.com/misitebao/wails-template-vue/compare/v2.0.0-beta.5...HEAD
[v2.0.0-beta.5]: https://github.com/misitebao/wails-template-vue/compare/v2.0.0-beta.4...v2.0.0-beta.5
[v2.0.0-beta.4]: https://github.com/misitebao/wails-template-vue/compare/v2.0.0-beta.3...v2.0.0-beta.4
